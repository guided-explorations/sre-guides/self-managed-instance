## GitLab Upgrade Version Selection Triage

How to determine which version of GitLab you would want to upgrade to.

Current GitLab Version:

Target Upgrade Candidate Version:

- [ ] 1. Any given GitLab upgrade can have special instructions associated with it. Check the list here: https://docs.gitlab.com/omnibus/update/#version-specific-changes

- [ ] 2. Upgrade Instructions: https://docs.gitlab.com/omnibus/update/ and leverage the upgrade path tool to find special upgrade instructions: https://gitlab-com.gitlab.io/support/toolbox/upgrade-path/

- [ ] 3. **Dependency Validation**: Review any dependency restrictions in your environment and understand how they interact with target GitLab versions.  For instance, if you are using PostgreSQL or Redis as a cloud service, do those services currently support the versions required by the version of GitLab you wish to move to? This is more important when a GitLab release is finally retiring a version of one of these dependencies.

     Current GitLab Version:

     

  | Dependency | Target GitLab Version Requires | Your Environment Supports | Update of Version Required <br/>for Target GitLab Version? |
  | ---------- | ------------------------------ | ------------------------- | ---------------------------------------------------- |
  | PostgreSQL |                                |                           |                                                      |
  | Redis      |                                |                           |                                                      |
  |            |                                |                           |                                                      |

  

- [ ] 4. Review what is new between your current version and prospective target version [using GitLabs 'What Is New' Since Tool](https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since/)

    > This information can be used for internal release notes to your developers of features you know might be of interest to your specific internal developer community.

- [ ] 5. Review Version Specific to CHANGELOG.md to:
     * look for features interesting to your organization (keep a list and promote to your developers in an upgrade announcement)
     * look for possible concerns for your specific GitLab implementation
     * look for fixes for bugs you know are a problem for your users (promote to your developers or those waiting on an internal ticket for this to be fixed)

    ```bash
  git clone git@gitlab.com:gitlab-org/gitlab-foss.git --depth 1000
  cd gitlab-foss
  git fetch --tags --depth 1000
  git diff v12.1.0 v11.0.0 -- CHANGELOG.md > ../CHANGELOG.diff.md
    ```

 - [ ] 6. Diff the template versions of the gitlab.rb of your current version with the version you are thinking of moving too.

     > It is recommend that you do not diff the new gitlab.rb version with your production version as there will be too many of your customizations in the diff to clearly see what has simply changed between releases.

     ```bash
   git clone git@gitlab.com:gitlab-org/omnibus-gitlab.git --depth 1000
   cd omnibus-gitlab
   git fetch --tags --depth 1000
   git diff 12.1.0+ee.0 11.0.0+ee.0 -- files/gitlab-config-template/gitlab.rb.template > ../gitlab.rb.diff.txt
     ```

 - [ ] 7. Once you have finalized your target version, splice all these diffs - including comments - to your production gitlab.rb.  This way the gitlab.rb "documentation as comments" will be accurate for the version of gitlab you upgrade to.

 - [ ] 8. Check the CHANGELOG.md for all regressions using this label search: [https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=✓&state=all&label_name[]=regression](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=✓&state=all&label_name[]=regression) 

 - [ ] 9. You can also check for a specific version regression by adding the version to the regression label like this (e.g. for version 12.9) : [https://gitlab.com/gitlab-org/gitlab/-/issues?label_name%5B%5D=regression%3A12.9](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name%5B%5D=regression%3A12.9)

 - [ ] 10. Check for Bugs using this label search: [https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=✓&state=opened&label_name[]=bug](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=✓&state=opened&label_name[]=bug)

 - [ ] 11. Consider doing a backup right before upgrade because if you need to revert to your previous version of the GitLab code you will also require a database restore to the schema version that matches the exact GitLab code version.

     Link to backup information: https://docs.gitlab.com/ee/raketasks/backup_restore.html
