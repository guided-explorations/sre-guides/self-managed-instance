
# SRE Guides: Self Managed Instance

## DevOps Checklists

1. [GitLab Upgrade Version Triage](/.gitlab/issue_templates/Selecting an Upgrade Version.md) - this is actually an issue template, so from that **Template Checklist** you can create a **Tracking Checklist** by clicking here: [Create a new issue from "Select an Upgrade Version"](https://gitlab.com/guided-explorations/sre-guides/self-managed-instance/-/issues/new?issuable_template=Selecting%20an%20Upgrade%20Version)


### DevOps Checklists In Issues / Merge Requests

Markdown Checklists can be used to managed DevOps operations, please see these articles:
* [Part 1: Why Human-Processed Source Code Belongs in Git](https://thenewstack.io/why-human-processed-source-code-belongs-in-git/)
* [Part 2: How Checklists on GitHub and GitLab Can Help Team Collaboration](https://thenewstack.io/part-2-how-checklists-on-github-and-gitlab-can-help-team-collaboration/)
* [Part 3: A Multiplatform Toolset for Markdown Checklists](https://thenewstack.io/human-processed-code-part-3-a-multiplatform-toolset-for-markdown-checklists/)
